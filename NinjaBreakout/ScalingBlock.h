#ifndef SCALING_BLOCK_H
#define SCALING_BLOCK_H
#include "Block.h"
class ScalingBlock : public Block
{
public:
	ScalingBlock();
	~ScalingBlock(){};
	void BlockHit(Entity&);
};
#endif