#include "Player.h"

Player::Player()
{
	maxAcceleration = 26;
	acceleration = 200;
	maxVelocity = 20;
	
	setColorFilter(graphicsNS::RED);
	animateSprite = true;
	//setCurrentFrame(3);
}
Player::~Player()
{

}

bool Player::Move(float frameTime)
{
	float xPos=getX();
	float yPos=getY();

	/*if(input != NULL)
	{
		if(input->getMouseX() < getX())
		{
			velocity.x -= acceleration*frameTime;
				return true;
		}
		else if(input->getMouseX() > getX()+getSWidth())
		{
			velocity.x += acceleration*frameTime;
				return true;
		}
	}*/

	bool bOutDeadZone = input->outDeadZone(0);

	if(input->isKeyDown(LEFT_KEY) || (input->getGamepadThumbLX(0) < 0 && bOutDeadZone))
		{	
			velocity.x = -20;//-= acceleration*frameTime;
			return true;
		}
		else if(input->isKeyDown(RIGHT_KEY) || (input->getGamepadThumbLX(0) > 0 && bOutDeadZone))
		{
			velocity.x =20;//+= acceleration*frameTime;
			return true;
		}

	return false;
}

void Player::Slowdown(float frameTime,float pAccel)
{
	if(velocity.x > 0 )
			{
				velocity.x -= (acceleration*pAccel)*frameTime;
				if(velocity.x < 0)
				{
					velocity.x = 0;
					if(!bShieldActive)
					setCurrentFrame(0);
				}
			}
			else if(velocity.x < 0)
			{
				velocity.x += (acceleration*pAccel)*frameTime;
				if(velocity.x > 0)
				{
					velocity.x = 0;
					if(!bShieldActive)
					setCurrentFrame(0);
				}
			}

}
void Player::update(float frameTime)
{

	if(input->isKeyDown(SPACEBAR_KEY) || input->getMouseRButton()==1 || input->getGamepadRightTrigger(0))
	{
		bShieldActive = true;
		animateSprite = false;
		setCurrentFrame(3);
		float slowRate = 0.3f;
		if(input->isKeyDown(LEFT_KEY) || input->isKeyDown(RIGHT_KEY))
		slowRate = 0.7f;

		Slowdown(frameTime,slowRate);
	}
	else
	{
		if(bShieldActive)
		{
			//setCurrentFrame(0);
			bShieldActive = false;
			animateSprite = true;
			setCurrentFrame(0);
		}
		setFrameDelay(1/Graphics::Vector2Length(&velocity)*4.0f);
		//If we are not pressing a button to move
		if(!Move(frameTime))
		{
		
			//If we are not moving, slow to a stop
			Slowdown(frameTime,1.2f);

		}

	}
	

	Entity::update(frameTime);
	velocity.x = clamp(velocity.x,-maxVelocity,maxVelocity);
	velocity.y = clamp(velocity.y,-maxVelocity,maxVelocity);
	
	float x = clamp(getX()+velocity.x,0,GAME_WIDTH-getSWidth());
	float y = clamp(getY()+velocity.y,0,GAME_HEIGHT);
	setX(x);
	setY(y);

}

void Player::onCollide(Entity& ent)
{

}

//void Player::draw()
//{
	//Image::draw(spriteData,graphicsNS::FILTER);
//}