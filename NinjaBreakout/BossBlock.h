#ifndef BOSS_BLOCK_H
#define BOSS_BLOCK_H
#include "Block.h"

class BossBlock : public Block
{
public:
	BossBlock();
	~BossBlock(){};

	void BlockHit(Entity&);
};
#endif