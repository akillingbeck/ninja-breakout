#ifndef BREAKOUT_H
#define BREAKOUT_H
#include <list>
#include <fstream>
#include <sstream>
#include "Scene.h"
#include "Entity.h"
#include "BlockIncludes.h"
#include "Player.h"
#include "TextureLibrary.h"
#include "Shuriken.h"
#include "textDX.h"


enum eBlockType {ONE_HIT=1, TWO_HIT, THREE_HIT,SCALING_UP,BOSS_BLOCK};

/*
Main gameplay class
*/

class Breakout : public Scene
{
	typedef void (Breakout::*Screen_update_ptr)(void);
private:
	float specialRecharge;
	float shurikenBonusTime; //Timer to apply a score each couple of seconds for how many shurikens you have
	float screenTimer;//timer to change screen
	float screenLoadTime;//How much time for a screen to stay active
	float specialAttackRateTimer;
	float throwTime;//Timer variable for allowing another shuriken to be thrown

	int airStrikeStartX;
	int playerScore;
	int lives;
	int totalSAttackShurikens;
	int sAttackCreated;
	int currentLevel; //Current Map level
	int maxBlocksX; //Maximum number of blocks along the x

	bool bSwitchIntroScreen;
	bool bIsAirstrike;//Special attack
	bool bSpecialFlash;
	bool bUsedSpecialAttack;
	bool bUsingSpecialAttack;
	bool bIsBossLevel;
	bool bFiredShuriken;

	
	Shuriken playerCollisionBlock; //Extra entity used for the collision ontop of the player so we can change its width/height independent of the player image
	Player* player;

	Image background;
	Image spBackground;
	Image livesSprites[4];
	Image specialBar;
	Image splashScreen;


	
	std::list<Entity*> blockMap;//List of all the blocks that are loaded from file
	std::list<MoveableEntity*> shurikens;//List of the active shurikens
	std::list<Entity*>::iterator it;
	std::list<MoveableEntity*>::iterator moveEntIt;
	
	TextDX* dxFont;
	TextDX* uiText;

	std::string BossText;
	std::string scoreText;
	std::string collisionDebug;
	std::string levelCompleteTxt;


	Screen_update_ptr updateScreen;
public:
	Breakout();
	~Breakout();

	 // Initialize the game
    void initialize(HWND hwnd);
	void initializeHandles();
	void initializePlayer();
	void initializeHUD();
	void initializeBackgrounds();

    void update();      // must override pure virtual from Game

	void drawLives();
	void drawSpecialBar();

	void updateIntro();
	void updateGame();
	void updateLevelComplete();
	void updateGameOver();

    void releaseAll();
    void resetAll();
	bool LoadMapEntities(int);
	void addBlock(int,int,int);

	void ThrowShuriken(float,bool isSpecial=false);


	//Collisions
	bool shurikenWithShield(Shuriken*,Entity* shield);
	bool shurikenWithPlayer(Shuriken*,Player* player);
	bool shurikenWithBlock(Shuriken*,Block* block);
	//Misc
	bool finishedLoading(float loadTime);
	bool checkLevelFailed();
	bool checkLevelComplete();
	void addShurikenBonus();
	void rechargeShurikenThrow();
	bool checkSpecialAttack();
	void doSpecialAttack();
	void rechargeSpecialAttack();
	void updateEntities();
	void addScore(int,bool hitShield=false);
};

#endif