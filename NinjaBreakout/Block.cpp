#include "Block.h"

Block::Block()
{
	blockLevel = 1;
	scalingTween = 0;
	animateSprite = false;
}
Block::~Block()
{
	int test = 0;
}
void Block::BlockHit(Entity& ent)
{
	if(blockLevel <= 0)
		isDirty = true;
}
void Block::onCollide(Entity& ent)
{
	animateSprite = true;
	setFrames(0,3);
	setFrameDelay(0.03f);
	BlockHit(ent);
}

void Block::draw()
{
	Image::draw(spriteData,initialColor);
}

void Block::update(float frameTime)
{
	//setY(getY() + 1);
	Image::update(frameTime);

	if(animComplete)//Stop the bounce animation from the block whenever it finishes
	{
		animateSprite = false;
		animComplete = false;
	}
}
