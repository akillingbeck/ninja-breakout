#ifndef BLOCK_INCS_H
#define BLOCK_INCS_H

#include "Block.h"
#include "SingleBlock.h"
#include "DoubleBlock.h"
#include "TripleBlock.h"
#include "ScalingBlock.h"
#include "BossBlock.h"

#endif