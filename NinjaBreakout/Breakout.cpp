#include "Breakout.h"

Breakout::Breakout()
{
	specialRecharge =0;
	currentLevel = 1;
	screenTimer = 0;
	screenLoadTime = 4;
	levelCompleteTxt = "LEVEL COMPLETE";
	bFiredShuriken = false;
	throwTime= 0;
	lives = 4;
	bUsedSpecialAttack =false;
	bUsingSpecialAttack = false;
	specialAttackRateTimer = 0;
	totalSAttackShurikens= 25;
	sAttackCreated =0;
	bSpecialFlash = false;
	playerScore = 0;
	shurikenBonusTime = 0;
	bIsBossLevel = false;
	bSwitchIntroScreen = false;
	bIsAirstrike = false;
	airStrikeStartX = 0;

}
Breakout::~Breakout()
{
    releaseAll();           // call onLostDevice() for every graphics item
}

void Breakout::initializePlayer()
{
	player = new Player();
	//Initialise player
	player->initialize(this,218,314,4,&TextureLibrary::playerTexture);
	player->setFrames(1,2);
	player->setFrameDelay(0.3f);

	player->setScale(0.5f);
	float y = GAME_HEIGHT -(player->getHeight()*player->getScale())*0.75f;
	player->setY(y);

	//Initialise this after player so we can parent it
	playerCollisionBlock.initialize(this,233,60,0,&TextureLibrary::blockTexture,player);
	playerCollisionBlock.setScale(0.5f);
	playerCollisionBlock.setLocalPosition(0,0.0f);


}
void Breakout::initializeHandles()
{
	dxFont = new TextDX();
	if(!dxFont->initialize(graphics,FONT_HEIGHT,true,false,FONT))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing font"));
	dxFont->setFontColor(graphicsNS::WHITE);

	uiText = new TextDX();
	if(!uiText->initialize(graphics,18,true,false,FONT))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing font"));
	uiText->setFontColor(graphicsNS::LIME);

	TextureLibrary::initialiseTextures(graphics);
}
void Breakout::initializeHUD()
{
	//Lives
	for(int j=lives;j>=0;j--)
	{
		livesSprites[j].initialize(graphics,0,0,0,&TextureLibrary::shurikenTexture);
		livesSprites[j].setScale(0.5f);
		livesSprites[j].setX(GAME_WIDTH - (livesSprites[j].getSWidth() * j) -(livesSprites[j].getSWidth()));
		livesSprites[j].setY(GAME_HEIGHT - livesSprites[j].getSHeight());
		livesSprites[j].setColorFilter(graphicsNS::RED);
	}

	 //Special bar
	specialBar.initialize(graphics,233,60,0,&TextureLibrary::blockTexture);
	specialBar.setColorFilter(graphicsNS::BLACK);
	specialBar.setX(0);
	specialBar.setY(GAME_HEIGHT-specialBar.getSHeight());

	//Score
	std::stringstream sstream;
	sstream<<"SCORE "<<playerScore;
	scoreText = sstream.str();

}
void Breakout::initializeBackgrounds()
{
	background.initialize(graphics,0,0,0,&TextureLibrary::backgroundTexture);
	background.setScale(1.0f);

	spBackground.initialize(graphics,0,0,0,&TextureLibrary::spcBackgroundTexture);
	spBackground.setScale(1.0f);

	if (!TextureLibrary::splashScreenTexture.initialize(graphics,"images\\ninjaBreakoutLogo.png"))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing logo texture"));

	splashScreen.initialize(graphics,0,0,0,&TextureLibrary::splashScreenTexture);
	splashScreen.setX(0);//GAME_WIDTH*0.5f - splashScreen.getSWidth()*0.5f);
}
void Breakout::initialize(HWND hwnd)
{
	Scene::initialize(hwnd); //Still want to initialize graphics/input etc from base class

	initializeHandles();
	initializePlayer();
	initializeHUD();
	initializeBackgrounds();

	updateScreen = &Breakout::updateIntro;
}

void Breakout::update()
{
	if (SUCCEEDED(graphics->beginScene()))
    {
		//This function pointer gets repointed to update the correct current "Screen"
		(this->*updateScreen)();

	 //stop rendering
        graphics->endScene();
    }
    handleLostGraphicsDevice();

    //display the back buffer on the screen
    graphics->showBackbuffer();
}

void Breakout::resetAll()
{
	Scene::resetAll();
	bIsAirstrike = false;
	airStrikeStartX = 0;
	sAttackCreated = 0;
	lives = 4;
	bSpecialFlash = false;
	bUsedSpecialAttack =false;
	bUsingSpecialAttack = false;
	bFiredShuriken = false;

	specialRecharge = 100;
	RECT tmp = specialBar.getSpriteDataRect();
	tmp.right = 233*(specialRecharge*0.01f);
	specialBar.setSpriteDataRect(tmp);


	//Clear previous list
	if(blockMap.size() > 0)
	{
		for(it = blockMap.begin(); it != blockMap.end();++it) 
		{
			SAFE_DELETE(*it);
		}
		blockMap.clear();
	}

	if(shurikens.size() > 0)
	{
		for(moveEntIt = shurikens.begin(); moveEntIt != shurikens.end();++moveEntIt) 
		{
			SAFE_DELETE(*moveEntIt);
		}
		shurikens.clear();
	}



}
void Breakout::releaseAll()
{
	Scene::releaseAll();
	SAFE_DELETE(dxFont);
	SAFE_DELETE(uiText);
	SAFE_DELETE(player);

	for(it = blockMap.begin(); it != blockMap.end();++it) 
	{
		SAFE_DELETE(*it);
	}

	for(moveEntIt = shurikens.begin(); moveEntIt != shurikens.end();++moveEntIt) 
	{
		SAFE_DELETE(*moveEntIt);
	}
}

void Breakout::addBlock(int p_blockType,int xOff,int yOff)
{
	eBlockType blockType = (eBlockType)p_blockType;
	//This horizSize is calculated from the longest "line" within the map file, it tells us how many spaces
	// we should allow within the screen for the blocks to fill. From this we get the width each block should be
	int horizSize = GAME_WIDTH / maxBlocksX;
	Block* block = NULL;

	bool noBlock = false;

	//Create a new blocktype based on what we loaded from the text file
	switch(blockType)
	{
	case ONE_HIT:
	{
		block = new SingleBlock();
		break;
	}
	case TWO_HIT:
		block = new DoubleBlock();
		break;
	case THREE_HIT:
		block = new TripleBlock();
		break;
	case SCALING_UP:
		block = new ScalingBlock();
		break;
	case BOSS_BLOCK:
		bIsBossLevel = true;
		block = new BossBlock();
		break;
	default:
		noBlock = true;
		break;
	}

	if(!noBlock)
	{
		block->initialize(this,233,60,1,&TextureLibrary::blockTexture);
		block->setReverse(true);
		block->setLoop(false);
		block->setFrames(0,0);
		block->setFrameDelay(0.09f);
		float width = (float)block->getWidth();
		float height = (float) block->getHeight();

		//Percentage we have to scale the original to fit the new size we need
			float scaleAmount = horizSize / width;
			block->setScale(scaleAmount);
			float scaledWidth = width * block->getScale();
			float scaledHeight = height * block->getScale();

			block->setX(xOff * scaledWidth);
			block->setY(yOff * scaledHeight);

			//if(audio->r

		blockMap.push_back(block);
	}

	block = NULL;
}
bool Breakout::LoadMapEntities(int level)
{
	bUsedSpecialAttack = false;
	lives = 4;


	std::ifstream mapIn;
	std::stringstream ss;
	ss<<"maps\\level"<<level<<".txt";
	bIsBossLevel = false;
	mapIn.open(ss.str());

	 if(!mapIn)
		 return false;
       // throw(GameError(gameErrorNS::FATAL_ERROR, "Error loading map data"));

	 std::string temp  = "";
	 
	maxBlocksX = 0;

	 if(mapIn.is_open())
	 {
		 while(getline(mapIn,temp))
		 {
			 int length = temp.size();
			 if(length > maxBlocksX)
				 maxBlocksX = length;  //Get the length of the longest line in the text file, so we can scale our blocks to fit in
		 }
		 mapIn.clear();
		 mapIn.seekg(0);

		 int xOffset=0,yOffset=0;
		 while(getline(mapIn,temp))
		 {
			 for(UINT i=0;i<temp.size();i++)
			 {
				 if(temp[i] != '*')
				 {
					 addBlock(temp[i]-'0',xOffset,yOffset);
				 }

				 xOffset++;
			 }
			 xOffset =0;
			 yOffset++;
		 }


		  mapIn.close();
		 
		  return true;
	 }

	 return false;
	
}

void Breakout::drawLives()
{
	for(int i=0;i<lives;i++)
	{
		livesSprites[i].draw(graphicsNS::FILTER);
	}
}
void Breakout::drawSpecialBar()
{
	specialBar.draw(graphicsNS::FILTER);
}


void Breakout::rechargeShurikenThrow()
{
	if(bFiredShuriken)
		{
			throwTime += frameTime;
			if(throwTime >= 0.2)
			{
				throwTime = 0;
				bFiredShuriken = false;
			}
		}
}
void Breakout::ThrowShuriken(float initY,bool isSpecial)
{
	audio->playCue(WHISH);
	bFiredShuriken = true;
	Shuriken* shuriken = new Shuriken();
	shuriken->initialize(this,0,0,0,&TextureLibrary::shurikenTexture);
	shuriken->setScale(0.5);

	shuriken->setRadius(shuriken->getSWidth()*0.35f);
	shuriken->setSuper(isSpecial);

	if(!bIsAirstrike)
	{
		shuriken->setX(player->getX());
		shuriken->setY(player->getY() - shuriken->getSHeight()*2.0f);
	}
	else
	{
		shuriken->setX(airStrikeStartX);
		shuriken->setY(player->getY() - shuriken->getSHeight()*2.0f);
		airStrikeStartX += (int)shuriken->getSWidth();
		shuriken->timesSuperCollide = 1;
	}
	//shuriken->setLocalPosition(0,-shuriken->getSHeight()*2);


	shurikens.push_back(shuriken);
	shuriken->update(frameTime);

	if(!bIsAirstrike)
	{
		shuriken->Launch(player->getVelX(),initY);
	}
	else
	{
		shuriken->Launch(0,initY);
	}

	shuriken = NULL;
			
	player->setCurrentFrame(1);
}
//Amount of score to add, and a flag whether or not the shield was hit so we can award only those points
//for the shield being active. No points are awarded other than that unless the shield is down. BE A NINJA
void Breakout::addScore(int amount,bool hitShield)
{
	if(hitShield || !player->getShieldActive())
	{
	playerScore += amount;
	std::stringstream sstream;
	sstream<<"SCORE "<<playerScore<<"\nx"<<shurikens.size();
	scoreText = sstream.str();
	}
}
//Add score bonus over time depending on the number of shurikens
void Breakout::addShurikenBonus()
{
	shurikenBonusTime+=frameTime;
	if(shurikenBonusTime >= 2)
	{
		shurikenBonusTime = 0;

		if(!player->getShieldActive())
		addScore(100*shurikens.size());
	}
}


bool Breakout::checkSpecialAttack()
{
	//if we are not using the special attack
	if(!bUsingSpecialAttack)
		{
			if(input->isKeyDown(UP_KEY) || input->getMouseLButton() ==1 || input->getGamepadA(0))
			{
				player->setCurrentFrame(0);
				if(!bFiredShuriken && !player->getShieldActive())
				{
					ThrowShuriken(1,false);
				}
			}

			if(!bUsedSpecialAttack)
				{
					if(input->isKeyDown(ENTER_KEY) || input->getGamepadY(0))
					{
						
							totalSAttackShurikens= 25;
							bIsAirstrike = false;
							bUsedSpecialAttack =true;
						
					}
					else if(input->getGamepadB(0))
					{
						
							
							totalSAttackShurikens= GAME_WIDTH / 40;
							bIsAirstrike = true;
							bUsedSpecialAttack =true;
							
						
					}

					if(bUsedSpecialAttack)//If special was used this frame
					{
						specialRecharge = 0;
						audio->playCue(HADOUKEN);
						bUsingSpecialAttack = true;
					}
				}
		}

	return bUsingSpecialAttack;

}
void Breakout::rechargeSpecialAttack()
{
		//increase specialRecharge by 10 every second
		specialRecharge += (10 * frameTime);

		RECT tmp = specialBar.getSpriteDataRect();
		tmp.right = 233*(specialRecharge*0.01f);
		specialBar.setSpriteDataRect(tmp);

		if(specialRecharge >= 100 )
		{
			specialRecharge = 0;
			bUsedSpecialAttack= false;
			airStrikeStartX = 0;
		}
	
}
void Breakout::doSpecialAttack()
{
	
		//Special attack
		specialAttackRateTimer+=frameTime;
		player->setCurrentFrame(0);
		if(specialAttackRateTimer >= 0.05)
		{
			bSpecialFlash = !bSpecialFlash;
			specialAttackRateTimer = 0;
			
			if(sAttackCreated < totalSAttackShurikens)
			{
				sAttackCreated++;
				ThrowShuriken(0.7f,true);
			}
			else
			{
				bIsAirstrike = false;
				airStrikeStartX = 0;
				bSpecialFlash = false;
				specialAttackRateTimer=0;
				bUsingSpecialAttack = false;
				sAttackCreated = 0;

			}
		}
}
//Collision functions for Shurikens
bool Breakout::shurikenWithBlock(Shuriken* shur,Block* block)
{
	
	VECTOR2 reflectValue; 
	reflectValue = VECTOR2(0,0);

	if(shur->collideBoxCircle(*block,reflectValue,shur->getSWidth()*2.0f))
	{
		shur->ChangeVelocity(reflectValue,VECTOR2(0,0));
		return true;
	}
	return false;
}
bool Breakout::shurikenWithShield(Shuriken* shur,Entity* shield)
{

	VECTOR2 reflectValue; 
	reflectValue = VECTOR2(0,0);

	if(player->getShieldActive())
	{
		if(shur->collideBoxCircle(*shield,reflectValue,shur->getSWidth()*4.0f))
		{
			
			//shur->setY(shield->getY() - shur->getHeight()*0.45f);
			shur->ChangeVelocity(reflectValue,player->getVelocity());
			return true;
		}
	}
	return false;
}
bool Breakout::shurikenWithPlayer(Shuriken* shur,Player* player)
{
	VECTOR2 reflectValue; 
	reflectValue = VECTOR2(0,0);

	if(player->getShieldActive() == true)
	{
		return false;
	}

	if(shur->collideBoxCircle(*player,reflectValue,shur->getSWidth()*4.0f))
	{
		shur->setDirty(true);
		return true;
	}
	return false;
}


void Breakout::updateEntities()
{
	int shurikenCount = shurikens.size();
	
		for(it = blockMap.begin(); it != blockMap.end();) 
			{
				Block* block = ((Block*)(*it));

				//If its active, not dirty and the distance between the shuriken and this block is less than 2, it will be a possible collision
				//so continue
				if(block->getActive() && block->getDirty()==false)
				{
					block->update(frameTime); 
					block->draw();

					for(moveEntIt = shurikens.begin(); moveEntIt != shurikens.end();)
					{
						 Shuriken* shur = ((Shuriken*)(*moveEntIt));
						 shur->bHasBounced = false;
						
						
						 
						 if(shur->getActive() && shur->getDirty() == false)
						 {
							 //If we haven't updated each shuriken at least once
							if(shurikenCount > 0)
							{
								if(!bUsingSpecialAttack || shur->getSuper())
								shur->update(frameTime);
								shur->draw();
								shurikenCount --;

								
									if(shurikenWithShield(shur,&playerCollisionBlock))
									{
										addScore(500,true);
										audio->playCue(METAL_HIT);
									}
									else if(shurikenWithPlayer(shur,player))
									{
										//only allow to hit if not using the special attack and dont have the shield active, otherwise we would get hit if another
										//special was active
										if(!bUsingSpecialAttack )
										{
						 				addScore(-500);
						 				audio->playCue(OUCH);
						 				lives--;
										}
									}
								
							}


						
								if(!shur->bHasBounced)//If this shuriken has not already hit a block in this frame
								{
							 					
							 			if(shurikenWithBlock(shur,block))
							 			{
							 				shur->bHasBounced = true;
							 				addScore(5);
							 				audio->playCue(WOBBLE);
							 			}
								}
							
							 ++moveEntIt;
						 }
						 else if(shur->getDirty())
						 {
							 delete *moveEntIt;
							 moveEntIt = shurikens.erase(moveEntIt);
						 }
					}
					++it;
				}
				else if(block->getDirty())
				{
					delete *it;
					it = blockMap.erase(it);
				}
				block = NULL;
			}
	
}
void Breakout::updateGame()
{

	 graphics->spriteBegin();                // begin drawing sprites

	 if(bSpecialFlash)
		 spBackground.draw();
	 else
		 background.draw();

	uiText->print(scoreText,(GAME_WIDTH*0.5f)-(scoreText.length()*9.0f),0);
	if(bIsBossLevel)
	{
		uiText->print("BOSS BLOCK",(GAME_WIDTH*0.5f)-(9.0f*9.0f),GAME_HEIGHT - 18);
	}

	


	addShurikenBonus();
	rechargeShurikenThrow();
	
	if(checkSpecialAttack())
	{
		doSpecialAttack();
	}
	else if(bUsedSpecialAttack)
	{
		rechargeSpecialAttack();
	}
	//Update player and the players collision block
	if(player != NULL && !bUsingSpecialAttack)
	{
		player->update(frameTime);
		player->draw();
		playerCollisionBlock.update(frameTime);
	}

	updateEntities();

	drawLives();
	drawSpecialBar();

	graphics->spriteEnd();

	if(checkLevelComplete() ||
	checkLevelFailed())
	{
		resetAll();
	}
	
}
void Breakout::updateIntro()
{

		if(bSwitchIntroScreen)
		{

			if(finishedLoading(2.0f))
			{
				bSwitchIntroScreen = false;
				currentLevel = 1;
				LoadMapEntities(currentLevel);
				updateScreen = &Breakout::updateGame;

				audio->playCue(BGMUSIC);
			}
		}
		if(input->isKeyDown(SPACEBAR_KEY) || input->getGamepadStart(0) && !bSwitchIntroScreen)
		{
			bSwitchIntroScreen = true;
			TextureLibrary::unloadTexture(TextureLibrary::splashScreenTexture);
			audio->playCue(HIYAH);
		}

	
        graphics->spriteBegin();         
		splashScreen.draw();
		dxFont->setFontColor(graphicsNS::BLACK);

		if(!bSwitchIntroScreen)
		dxFont->print("Press Start",GAME_WIDTH*0.5f - (36*5),GAME_HEIGHT-72);
	
		graphics->spriteEnd();

}
void Breakout::updateLevelComplete()
{
	

	if(finishedLoading(3.0f))
	{
		specialRecharge=0;
		if(!LoadMapEntities(currentLevel))
		{
			currentLevel = 1;

			audio->stopCue(BGMUSIC);
			//Simple but ugly, duplicates also in another place.
			if (!TextureLibrary::splashScreenTexture.initialize(graphics,"images\\ninjaBreakoutLogo.png"))
				throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing logo texture"));

			updateScreen = &Breakout::updateIntro;
		}
		else
		{
			addScore(0);
			updateScreen = &Breakout::updateGame;
		}
	}

	 graphics->spriteBegin();                // begin drawing sprites

	 background.draw();

	 dxFont->setFontColor(graphicsNS::WHITE);
	 dxFont->print(levelCompleteTxt,GAME_WIDTH*0.25f,GAME_HEIGHT*0.5f);
	 dxFont->setFontColor(graphicsNS::BLACK);
	 dxFont->print(levelCompleteTxt,GAME_WIDTH*0.248f,GAME_HEIGHT*0.498f);

	  graphics->spriteEnd();
}
void Breakout::updateGameOver()
{

	if(finishedLoading(3.0f))
	{
		if (!TextureLibrary::splashScreenTexture.initialize(graphics,"images\\ninjaBreakoutLogo.png"))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing logo texture"));

		screenTimer = 0;
		lives = 4;

		audio->stopCue(BGMUSIC);
		currentLevel = 1;
		playerScore=0;
		updateScreen = &Breakout::updateIntro;
	
	}

	graphics->spriteBegin();                // begin drawing sprites

	 background.draw();

	 
	 dxFont->setFontColor(graphicsNS::WHITE);
	 dxFont->print(levelCompleteTxt,GAME_WIDTH*0.25f,GAME_HEIGHT*0.5f);
	 dxFont->setFontColor(graphicsNS::BLACK);
	  dxFont->print(levelCompleteTxt,GAME_WIDTH*0.248f,GAME_HEIGHT*0.498f);

	  graphics->spriteEnd();
}


bool Breakout::finishedLoading(float loadTime)
{
	screenTimer+=frameTime;

	if(screenTimer >= loadTime)
	{
		screenTimer = 0;
		return true;
	}

	return false;
}

bool Breakout::checkLevelFailed()
{
	if(lives < 0)
		{
			updateScreen = &Breakout::updateGameOver;
			std::stringstream sstream;
			sstream<<"LEVEL "<<currentLevel<<" FAILED!!";
			currentLevel = 1;
		
			levelCompleteTxt = sstream.str();

			return true;
		}

	return false;
}

bool Breakout::checkLevelComplete()
{
	if(blockMap.size() == 0)
		{
			updateScreen = &Breakout::updateLevelComplete;
			std::stringstream sstream;
			sstream<<"LEVEL "<<currentLevel<<" COMPLETE";
			currentLevel++;
		
			levelCompleteTxt = sstream.str();
			//dxFont->initialize(graphics,72,true,false,FONT);
			return true;
		}

	return false;
}