#ifndef PLAYER_H
#define PLAYER_H
#include "MoveableEntity.h"

class Player : public MoveableEntity
{
protected:
	bool bShieldActive;
	float maxAcceleration;
public:
	Player();
	~Player();

	void update(float);
	bool Move(float);
	void onCollide(Entity&);
	virtual bool getShieldActive() const     {return bShieldActive;}
	void Slowdown(float,float);
	//void draw();
};

#endif