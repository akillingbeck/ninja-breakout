#ifndef SHURIKEN_H
#define SHURIKEN_H
#include "MoveableEntity.h"

class Shuriken : public MoveableEntity
{
protected:
	bool bWasThrown;
	float restTime;
	float timeRested;
	float degreeRot;
	

public:
	bool bHasBounced;
	int timesSuperCollide;
	Shuriken();
	~Shuriken();

	bool Move(float);
	void update(float);
	void draw();
	void Launch(float,float);
	void ChangeVelocity(VECTOR2 xyDir,VECTOR2 additionalVelocity);
};


#endif