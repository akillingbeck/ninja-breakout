#include "BossBlock.h"
#include <ctime>
BossBlock::BossBlock()
{
	blockLevel = 40;
	initialColor = graphicsNS::WHITE;
	srand((unsigned)time(NULL));
}

void BossBlock::BlockHit(Entity& ent)
{
	blockLevel--;

	if(blockLevel < 20 && blockLevel > 5)
	{
		initialColor = graphicsNS::ORANGE;
	}
	else if(blockLevel <= 5)
	{
		initialColor = graphicsNS::RED;
	}

	Block::BlockHit(ent);

	float randX = (float)(rand()% (int)(GAME_WIDTH-getSWidth()));

	if(randX<0)
		randX = 0;

	setX(randX);
	
	//This block eats shurikens when you damage it
	ent.bWasDestroyed = true;
	ent.setDirty(true);
}