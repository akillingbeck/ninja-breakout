#ifndef TEXTURE_LIB
#define TEXTURE_LIB
#include "textureManager.h"
#include "constants.h"

class TextureLibrary
{
public:
	TextureLibrary(){}
	~TextureLibrary(){}

	static TextureManager playerTexture;
	static TextureManager blockTexture;
	static TextureManager shurikenTexture;
	static TextureManager backgroundTexture;
	static TextureManager spcBackgroundTexture;

	static TextureManager splashScreenTexture;
	static TextureManager infoScreenTexture;

	static void initialiseTextures(Graphics* g);
	static void unloadTexture(TextureManager& tMan);
};

#endif