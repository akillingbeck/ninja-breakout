#include "DoubleBlock.h"
DoubleBlock::DoubleBlock()
{
	blockLevel = 2;
	initialColor = graphicsNS::ORANGE;
}

void DoubleBlock::BlockHit(Entity& ent)
{
	blockLevel--;
	//initialColor = graphicsNS::YELLOW;


	if(ent.getInitialColor() != graphicsNS::BLUE)
	ent.setInitialColor(graphicsNS::BLUE);
	else
	{
		ent.setInitialColor(graphicsNS::GREEN);
	}
	Block::BlockHit(ent);
}