#ifndef ENTITY_H
#define ENTITY_H

#include "image.h"
#include "input.h"
#include "Scene.h"
#include <math.h>

class Entity : public Image
{
protected:
	Entity* parent;
	Input   *input;         // pointer to the input system
	RECT    edge;           // for BOX and ROTATED_BOX collision detection
	VECTOR2 center;         // center of entity
	VECTOR2 distSquared;    // used for calculating circle collision
	VECTOR2 localPosition;
	VECTOR2 velocity;

    float  sumRadiiSquared;
	float radius;
	float acceleration;
	float maxVelocity;
	float maxAcceleration;


	bool    active;         // only active entities may collide
	bool isDirty; //flag to set when this entity should be removed from lists/memory

	Scene* scenePtr;
	DWORD initialColor;
	
	

public:
	bool bWasDestroyed;
	Entity();
	virtual ~Entity();

	inline float clamp(float x, float a, float b)
	{

		return x < a ? a : (x > b ? b : x);
	}

	virtual bool broadPhaseDistance(Entity &ent,float minDistance);
	virtual bool collideBoxCircle(Entity &ent,VECTOR2& CollisionPoint,float minDistance);

	virtual const VECTOR2* getCenter()   
    {
        center = VECTOR2(getCenterX(),getCenterY());
        return &center;
    }

	virtual void ChangeVelocity(VECTOR2 xyDir,VECTOR2 additionalVelocity);
	// Return radius of collision circle.
	virtual float getRadius()     {return radius;}

	virtual void setRadius(float rad)    {radius=rad;}

	virtual const VECTOR2 getVelocity() const {return velocity;}

	virtual void setVelocity(VECTOR2 newVelocity)  {velocity = newVelocity;}

	 virtual bool initialize(Scene *scenePtr, int width, int height, int ncols,
                            TextureManager *textureM);
	 virtual bool initialize(Scene *scenePtr, int width, int height, int ncols,
                            TextureManager *textureM,Entity* ent);

	virtual void setParent(Entity* parent){this->parent = parent;}

	void setEdge(RECT e) {edge = e;}

	virtual void setLocalPosition(FLOAT x,FLOAT y){localPosition.x = x;localPosition.y = y;}

	virtual float getLocalX(){return localPosition.x;}

	virtual float getLocalY(){return localPosition.y;}

	virtual const RECT& getEdge() const {return edge;}

	virtual void setDirty(bool dirt){isDirty=dirt;}
	  // Return active.
    virtual bool  getActive()         const {return active;}

	virtual bool  getDirty()         const {return isDirty;}

	virtual void update(float frameTime);

	virtual void onCollide(Entity&);

	virtual void setInitialColor(DWORD newColor){initialColor=newColor;}
	virtual const DWORD getInitialColor() const {return initialColor;}

	virtual void draw();

};

#endif