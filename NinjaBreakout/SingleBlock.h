#ifndef SINGLE_BLOCK_H
#define SINGLE_BLOCK_H
#include "Block.h"

class SingleBlock : public Block
{
public:
	SingleBlock();
	~SingleBlock();

	void BlockHit(Entity&);
};
#endif