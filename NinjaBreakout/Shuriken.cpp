#include "Shuriken.h"

Shuriken::Shuriken()
{
	initialColor = graphicsNS::WHITE;
	bWasThrown = false;
	maxVelocity = 12;
	speed = 12;

	restTime = 0.5;
	timeRested = 0;
	degreeRot = 0;
	timesSuperCollide =2;
	bHasBounced = true;
}
Shuriken::~Shuriken()
{
}

bool Shuriken::Move(float frameTime)
{
	//Check with screen bounds and set velocities
	if(getCenterX() > GAME_WIDTH)
	{
		velocity.x = -1 * speed;
	}
	else if(getCenterX() < 0)
	{
		velocity.x = 1 * speed;
	}

	if(getCenterY() > GAME_HEIGHT)
	{
		velocity.y = -1 * speed;
	}
	else if(getCenterY() < 0)
	{
		velocity.y = 1 * speed;
	}

	//Set rotation speed and direction based on the velocity
	float actualSpeed = Graphics::Vector2Length(&velocity);
	if(velocity.x>0)
	{
		degreeRot += (90*actualSpeed) * frameTime;
	}
	else
	{
		degreeRot -= (90*actualSpeed) * frameTime;
	}
	setDegrees(degreeRot);

	return false;
}

void Shuriken::update(float frameTime)
{
	
	if(bWasThrown)
	{
		Move(frameTime);
	}

	MoveableEntity::update(frameTime);

	//If this shuriken was shrank enough, delete it
	if(getScale() <= 0)
	{
		isDirty = true;
	}

}

void Shuriken::draw()
{
	Image::draw(spriteData,initialColor);
}

void Shuriken::Launch(float xDir, float yDir)
{
	velocity.x = xDir*speed;
	velocity.y = (yDir*-1)*speed;
	parent = NULL;
	bWasThrown = true;
}
//Should only be able to change velocity once every set amount of time
void Shuriken::ChangeVelocity(VECTOR2 xyDir,VECTOR2 additionalVelocity)
{
	float previousSpeed = graphics->Vector2Length(&velocity);
	if(getSuper())
	{
		timesSuperCollide--;
		if(timesSuperCollide <=0)
			isDirty=true;
	}
	else
	{
	velocity.x *= xyDir.x;// *previousSpeed;
	velocity.y *= xyDir.y;// * previousSpeed;
	velocity.x += additionalVelocity.x;
	velocity.y += additionalVelocity.y;
	}

}
