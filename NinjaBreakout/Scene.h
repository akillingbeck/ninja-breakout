// Programming 2D Scenes
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 Scene.h v1.0

#ifndef _Scene_H                 // Prevent multiple definitions if this 
#define _Scene_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <Mmsystem.h>
#include "graphics.h"
#include "audio.h"
#include "input.h"
#include "constants.h"
#include "gameError.h"
namespace SceneNS
{
    const char FONT[] = "Courier New";  // font
    const int POINT_SIZE = 14;          // point size
    const COLOR_ARGB FONT_COLOR = SETCOLOR_ARGB(255,255,255,255);    // white
}
class Scene
{
protected:
	
    // common Scene properties
    Graphics *graphics;         // pointer to Graphics
	Input   *input;             // pointer to Input
	Audio   *audio;             // pointer to Audio
    HWND    hwnd;               // window handle
    HRESULT hr;                 // standard return type
    LARGE_INTEGER timeStart;    // Performance Counter start value
    LARGE_INTEGER timeEnd;      // Performance Counter end value
    LARGE_INTEGER timerFreq;    // Performance Counter frequency

    float   frameTime;          // time required for last frame
    float   fps;                // frames per second
    DWORD   sleepTime;          // number of milli-seconds to sleep between frames

    bool    paused;             // true if Scene is paused
    bool    initialized;



public:
    // Constructor
    Scene();
    // Destructor
    virtual ~Scene();

    // Member functions

    // Window message handler
    LRESULT messageHandler( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam );

    // Initialize the Scene
    // Pre: hwnd is handle to window
    virtual void initialize(HWND hwnd);

    // Call run repeatedly by the main message loop in WinMain
    virtual void run(HWND);

    // Call when the graphics device was lost.
    // Release all reserved video memory so graphics device may be reset.
    virtual void releaseAll();

    // Recreate all surfaces and reset all entities.
    virtual void resetAll();

    // Delete all reserved memory.
    virtual void deleteAll();

    // Handle lost graphics device
    virtual void handleLostGraphicsDevice();

    // Set display mode (fullscreen, window or toggle)
    void setDisplayMode(graphicsNS::DISPLAY_MODE mode = graphicsNS::TOGGLE);

    // Return pointer to Graphics.
    Graphics* getGraphics() {return graphics;}

    // Return pointer to Input.
    Input* getInput()       {return input;}

    // Exit the Scene
    void exitScene()         {PostMessage(hwnd, WM_DESTROY, 0, 0);}


    // Pure virtual function declarations
    // These functions MUST be written in any class that inherits from Scene

    // Update Scene items.
    virtual void update() = 0;


};

#endif
