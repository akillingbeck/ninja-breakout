#ifndef TRIPLE_BLOCK_H
#define TRIPLE_BLOCK_H
#include "Block.h"
class TripleBlock : public Block
{
public:
	TripleBlock();
	~TripleBlock(){};
	void BlockHit(Entity&);
};
#endif