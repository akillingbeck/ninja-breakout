#include "MoveableEntity.h"

MoveableEntity::MoveableEntity()
{
	velocity.x = 0;
	velocity.y = 0;
	acceleration = 0;
	maxVelocity = 3;
	speed = 3;
	bIsSpecial = false;
}

MoveableEntity::~MoveableEntity()
{
}

void MoveableEntity::update(float frameTime)
{
	Entity::update(frameTime);

	velocity.x = clamp(velocity.x,-maxVelocity,maxVelocity);
	velocity.y = clamp(velocity.y,-maxVelocity,maxVelocity);
		
	setX(getX()+velocity.x);
	setY(getY()+velocity.y);
}

bool MoveableEntity::Move(float frameTime)
{
	return false;
}