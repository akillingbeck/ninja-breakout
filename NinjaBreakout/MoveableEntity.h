#ifndef MOVEABLE_ENTITY_H
#define MOVEABLE_ENTITY_H
#include "Entity.h"

class MoveableEntity : public Entity
{

protected:
	float speed;
		bool isSuperShuriken;
public:

	MoveableEntity();
	~MoveableEntity();

	bool bIsSpecial; //Was this created by the player for a special attack

	virtual float getVelX(){return velocity.x;}
	virtual float getVelY(){return velocity.y;}
	virtual bool getSuper(){return isSuperShuriken;}
	virtual void setSuper(bool is){isSuperShuriken=is;}

	virtual bool Move(float);
	virtual void update(float);



};
#endif