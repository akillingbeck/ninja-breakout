#ifndef DOUBLE_BLOCK_H
#define DOUBLE_BLOCK_H
#include "Block.h"
class DoubleBlock : public Block
{
public:
	DoubleBlock();
	~DoubleBlock(){};
	void BlockHit(Entity&);
};
#endif