#include "TripleBlock.h"
TripleBlock::TripleBlock()
{
	blockLevel = 3;
	initialColor = graphicsNS::RED;
}

void TripleBlock::BlockHit(Entity& ent)
{
	blockLevel--;



	if(initialColor == graphicsNS::RED)
	{
		setInitialColor(graphicsNS::LIME);
	}
	else if(initialColor == graphicsNS::LIME)
	{
		setInitialColor(graphicsNS::BLUE);
	}
	Block::BlockHit(ent);
}