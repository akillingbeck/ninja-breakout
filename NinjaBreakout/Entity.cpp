#include "Entity.h"

Entity::Entity()
{
	
	edge.left = -1;
    edge.top = -1;
    edge.right = 1;
    edge.bottom = 1;
	active = true; 
	isDirty= false;
	initialColor = graphicsNS::WHITE;
	setColorFilter(initialColor);
	parent= NULL;
	localPosition.x = 0;
	localPosition.y = 0;
	radius = 1.0;
	sumRadiiSquared = 0;
	bWasDestroyed = false;
}
Entity::~Entity()
{
	int test = 0;
	input = NULL;
	scenePtr = NULL;
	parent = NULL;
}

bool Entity::initialize(Scene *gamePtr, int width, int height, int ncols,
                           TextureManager *textureM,Entity* parent)
{
	this->parent = parent;

	return initialize(gamePtr,width,height,ncols,textureM);
}
bool Entity::initialize(Scene *gamePtr, int width, int height, int ncols,
                           TextureManager *textureM)
{
	this->scenePtr = gamePtr;

    input = gamePtr->getInput();                // the input system

    return(Image::initialize(gamePtr->getGraphics(), width, height, ncols, textureM));
}

bool Entity::broadPhaseDistance(Entity& ent, float minDistance)
{
	VECTOR2 Distance = *ent.getCenter() - *getCenter();

	return abs(graphics->Vector2Length(&Distance)) < minDistance;
}
//Mindistance used for broad collision
bool Entity::collideBoxCircle(Entity& ent, VECTOR2 &DirectionChange,float minDistance)
{
	if(broadPhaseDistance(ent,minDistance))//Broad phase test
	{
		float entHscaledWidth = (ent.getWidth() * ent.getScale())*0.5f;
		float entHscaledHeight = (ent.getHeight() * ent.getScale())*0.5f;
		VECTOR2 CollisionPoint;

		// Find the closest point to the circle within the rectangle
		float closestX = clamp(getCenterX(),
						ent.getCenterX() + ent.getEdge().left*entHscaledWidth,
						ent.getCenterX() + ent.getEdge().right*entHscaledWidth);
		float closestY = clamp(getCenterY(), 
						ent.getCenterY() + ent.getEdge().top*entHscaledHeight,
						ent.getCenterY() + ent.getEdge().bottom*entHscaledHeight);

		// Calculate the distance between the circle's center and this closest point
		float distanceX =getCenterX() - closestX;
		float distanceY = getCenterY() - closestY;

		// If the distance is less than the circle's radius, an intersection occurs
		float distanceSquared = (distanceX * distanceX) + (distanceY * distanceY);
		if(distanceSquared < (getRadius() * getRadius()))
		{
			CollisionPoint = VECTOR2(closestX,closestY);
			VECTOR2 CollisionVector = *getCenter() - CollisionPoint;
			graphics->Vector2Normalize(&CollisionVector);

			//Set the position of this to be previous to the collision point ( stops objects passing through / colliding twice)
			setX(getX() + (CollisionVector.x*getSWidth()*0.5f));
			setY(getY() + (CollisionVector.y*getSHeight()*0.5f));

			//Find what "side" of the rectangle the circle has intersected to decide a Normal direction
			bool left = getCenterX()-	(ent.getCenterX() + ent.getEdge().left*entHscaledWidth)   <= 0;
			bool right = getCenterX() - (ent.getCenterX() + ent.getEdge().right*entHscaledWidth) >= 0;
			bool top = getCenterY()-(ent.getCenterY() + ent.getEdge().top*entHscaledHeight)  <= 0;
			bool bottom = getCenterY()-(ent.getCenterY() + ent.getEdge().bottom*entHscaledHeight)  >= 0;

			//Simple solution to change direction after collision

			DirectionChange = VECTOR2(1,1);
			//If a collision on the top or bottom, reverse the Y direction
			if(bottom || top)
			{
				DirectionChange = VECTOR2(1,-1);
			}
			//If a collision on the left or right, reverse the X direction
			else if(left || right)
			{
				DirectionChange = VECTOR2(-1,1);
			}
	
			//onCollide(ent);
			ent.onCollide(*this);
			return true;
		}
	}

	return false;
}

void Entity::update(float frameTime)
{
	Image::update(frameTime);

	if(parent != NULL)
	{
		setX(parent->getX() + getLocalX());
		setY(parent->getY()+ getLocalY());
	}

}
void Entity::onCollide(Entity& ent)
{
}
void Entity::draw()
{
	Image::draw();
}

void Entity::ChangeVelocity(VECTOR2 xyDir,VECTOR2 additionalVelocity)
{

}