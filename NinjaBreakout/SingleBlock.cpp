#include "SingleBlock.h"

SingleBlock::SingleBlock()
{
	blockLevel = 1;
	initialColor = graphicsNS::YELLOW;
	setColorFilter(initialColor);
}
SingleBlock::~SingleBlock()
{

}

void SingleBlock::BlockHit(Entity& ent)
{
	blockLevel--;
	
	Block::BlockHit(ent);
	
}