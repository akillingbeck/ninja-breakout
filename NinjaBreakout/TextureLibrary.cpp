#include "TextureLibrary.h"

TextureManager TextureLibrary::playerTexture;
TextureManager TextureLibrary::blockTexture;
TextureManager TextureLibrary::shurikenTexture;
TextureManager TextureLibrary::backgroundTexture;
TextureManager TextureLibrary::spcBackgroundTexture;

TextureManager TextureLibrary::splashScreenTexture;
TextureManager TextureLibrary::infoScreenTexture;

//Single place to load all textures needed to memory
 void TextureLibrary::initialiseTextures(Graphics* g)
{
	
	if (!playerTexture.initialize(g,"images\\characterSheet.png"))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player texture"));

	if (!blockTexture.initialize(g,"images\\blockSheet.png"))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing block texture"));

	if (!shurikenTexture.initialize(g,"images\\shuriken.png"))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing block texture"));

	if (!backgroundTexture.initialize(g,"images\\dojo.jpg"))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing background texture"));

	if (!spcBackgroundTexture.initialize(g,"images\\vortex.jpg"))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing background texture"));

	
}

 void TextureLibrary::unloadTexture(TextureManager& tMan)
 {
	 tMan.onLostDevice();
 }