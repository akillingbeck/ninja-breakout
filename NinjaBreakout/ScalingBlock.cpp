#include "ScalingBlock.h"
ScalingBlock::ScalingBlock()
{
	blockLevel = 3;
	initialColor = graphicsNS::PURPLE;
}

void ScalingBlock::BlockHit(Entity& ent)
{
	blockLevel--;
	//initialColor = graphicsNS::YELLOW;
	/*setX(getX() + getSWidth()*0.1);
	setY(getY() + getSHeight()*0.1);
	setScale(getScale() - 0.1);*/

	ent.setX(ent.getX() + ent.getSWidth() * 0.1f);
	ent.setY(ent.getY() + ent.getSHeight() * 0.1f);
	ent.setScale(ent.getScale() - 0.1f);
	
	Block::BlockHit(ent);
}