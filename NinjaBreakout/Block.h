#ifndef BLOCK_H
#define BLOCK_H
#include "Entity.h"
class Block : public Entity
{
protected:
	float scalingTween;
	int blockLevel; //How many times it is hit before it dissapears
public:
	Block();
	~Block();

	virtual void BlockHit(Entity&);

	void onCollide(Entity&);
	void draw();
	void update(float);



};
#endif